extern crate proc_macro;
use proc_macro::TokenStream;
use proc_macro2::{TokenStream as TokenStream2, TokenTree};
use quote::quote;
use syn::{parse_macro_input, Attribute, Data, DataStruct, DeriveInput};

#[derive(Copy, Clone)]
enum DerivedTrait {
    View,
    Partial,
}

#[proc_macro_derive(TeraView, attributes(partial, template, exclude_from_template))]
pub fn derive_tera_view(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item);
    derive_tera_trait_inner(input, DerivedTrait::View)
        .unwrap_or_else(|e| e.into_compile_error())
        .into()
}

#[proc_macro_derive(TeraPartial, attributes(partial_name, partial, exclude_from_template))]
pub fn derive_tera_partial(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item);
    derive_tera_trait_inner(input, DerivedTrait::Partial)
        .unwrap_or_else(|e| e.into_compile_error())
        .into()
}

enum FieldType {
    Data,
    Partial { statement: TokenStream2 },
    Excluded,
}

fn derive_tera_trait_inner(
    input: DeriveInput,
    derived_trait: DerivedTrait,
) -> Result<TokenStream2, syn::Error> {
    let name = input.ident;
    let data = check_struct(input.data, derived_trait)?;
    let generics = input.generics;
    let attrs = input.attrs;

    let mut statements = Vec::new();

    let mut template_path = None;
    let mut partial_name = None;
    for attr in attrs {
        if attr.path.is_ident("template") {
            check_no_conflict(template_path, "template path", &attr)?;
            template_path = Some(parse_name_litstr_attr(attr)?);
        } else if attr.path.is_ident("partial_name") {
            check_no_conflict(partial_name, "partial name", &attr)?;
            partial_name = Some(parse_name_litstr_attr(attr)?);
        } else if attr.path.is_ident("partial") {
            let partial = parse_args_attr(attr)?;
            statements.push(quote! {
                (#partial).add_to(context);
            });
        } else if attr.path.is_ident("exclude_from_template") {
            return Err(unexpected_attribute_err(attr));
        }
    }

    let partial_name = partial_name.unwrap_or_else(|| name.to_string().to_lowercase());

    for field in data.fields {
        if let Some(field_name) = field.ident {
            let mut field_type = FieldType::Data;
            for attr in field.attrs {
                if attr.path.is_ident("partial") {
                    let statement = match field_type {
                        FieldType::Excluded => return Err(unexpected_attribute_err(attr)),
                        FieldType::Partial { .. } => return Err(unexpected_attribute_err(attr)),
                        FieldType::Data => match attr.tokens.is_empty() {
                            true => quote! { self.#field_name.add_to(context); },
                            false => {
                                let partial = parse_args_attr(attr)?;
                                quote! { (#partial).add_to(context); }
                            }
                        },
                    };
                    field_type = FieldType::Partial { statement };
                } else if attr.path.is_ident("exclude_from_template") {
                    match field_type {
                        FieldType::Excluded => return Err(unexpected_attribute_err(attr)),
                        FieldType::Partial { .. } => return Err(unexpected_attribute_err(attr)),
                        FieldType::Data => field_type = FieldType::Excluded,
                    }
                } else if attr.path.is_ident("template") || attr.path.is_ident("partial_name") {
                    return Err(unexpected_attribute_err(attr));
                }
            }

            match field_type {
                FieldType::Data => {
                    let inserted_name = match derived_trait {
                        DerivedTrait::View => field_name.to_string(),
                        DerivedTrait::Partial => format!("_{}__{}", partial_name, field_name),
                    };
                    statements.push(quote! { context.insert(#inserted_name, &self.#field_name); });
                }
                FieldType::Excluded => (),
                FieldType::Partial { statement } => statements.push(statement),
            }
        }
    }

    let output = match derived_trait {
        DerivedTrait::View => {
            let template_path = template_path
                .ok_or_else(|| syn::Error::new(name.span(), "missing #[template] for this view"))?;
            quote! {
            impl #generics TeraView for #name #generics {
                const PATH: &'static str = #template_path;
                fn add_to(self, context: &mut tera::Context) {
                    #(#statements)*
                }
            }}
        }
        DerivedTrait::Partial => quote! {
            impl #generics TeraPartial for #name #generics {
                fn add_to(self, context: &mut tera::Context) {
                    #(#statements)*
                }
            }
        },
    };
    Ok(output)
}

fn check_struct(data: Data, derived_trait: DerivedTrait) -> Result<DataStruct, syn::Error> {
    let msg = match derived_trait {
        DerivedTrait::View => "TeraView derive macro can only be applied to a struct",
        DerivedTrait::Partial => "TeraPartial derive macro can only be applied to a struct",
    };
    match data {
        syn::Data::Struct(data) => Ok(data),
        syn::Data::Enum(data) => Err(syn::Error::new(data.enum_token.span, msg)),
        syn::Data::Union(data) => Err(syn::Error::new(data.union_token.span, msg)),
    }
}

fn check_no_conflict(data: Option<String>, what: &str, attr: &Attribute) -> Result<(), syn::Error> {
    match data {
        Some(_) => Err(syn::Error::new(
            attr.bracket_token.span,
            format!("conflicting {} definition", what),
        )),
        None => Ok(()),
    }
}

fn parse_name_litstr_attr(attr: syn::Attribute) -> Result<String, syn::Error> {
    match attr.parse_meta()? {
        syn::Meta::NameValue(mnv) => match mnv.lit {
            syn::Lit::Str(lit_str) => Ok(lit_str.value()),
            lit => Err(syn::Error::new(lit.span(), "expected a string literal")),
        },
        _ => Err(syn::Error::new(
            attr.bracket_token.span,
            format!(
                "expected `=` after `{}`",
                attr.path.segments.last().unwrap().ident
            ),
        )),
    }
}

fn parse_args_attr(attr: syn::Attribute) -> Result<TokenStream2, syn::Error> {
    let mut iter = attr.tokens.into_iter();
    let args = if let Some(TokenTree::Group(g)) = iter.next() {
        g.stream()
    } else {
        return Err(syn::Error::new(
            attr.bracket_token.span,
            &format!(
                "expected `{}(..)`",
                attr.path.segments.last().unwrap().ident
            ),
        ));
    };
    if let Some(tt) = iter.next() {
        return Err(syn::Error::new(
            tt.span(),
            "Unexpected additional token tree",
        ));
    }
    Ok(args)
}

fn unexpected_attribute_err(attr: syn::Attribute) -> syn::Error {
    syn::Error::new(
        attr.bracket_token.span,
        format!(
            "unexpected attribute `{}` here",
            attr.path.segments.last().unwrap().ident
        ),
    )
}
