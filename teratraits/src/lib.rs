//! # Tera traits
//!
//! Set of Rust `trait`s and derive macros to represent Tera views and partials.
//!
//! The goal of these traits is to let the user define a `struct` for each view/partial,
//! implementing the appropriate `trait`, so that the `struct` provides a type-safe interface which
//! can be unit tested to match the data required by the Tera template by simply rendering it.
//!
//! See [`TeraView`] and [`TeraPartial`] for manual implementation examples.
//!
//! See [`derive(TeraView)`](teratraits_derive::TeraView) and
//! [`derive(TeraPartial)`](teratraits_derive::TeraPartial) for derived implementation examples.

#![warn(missing_docs)]

#[allow(unused_imports)]
#[macro_use]
extern crate teratraits_derive;

/// Derive macro to implement [`TeraView`].
///
/// - Fields without attributes are inserted into the Tera template context as variables named as
///   their field names.
/// - Partials are inserted using [`.add_to(context)`](TeraPartial::add_to).
///
/// # Examples
///
/// ```
/// use teratraits::{TeraView, TeraPartial};
/// use serde::Serialize;
///
/// // From the model
/// #[derive(Serialize)]
/// struct User;
///
/// // The view extends a `Base` partial defined elsewhere
/// #[derive(TeraPartial)]
/// pub struct Base<'a> {
///     title: &'a str,
/// }
///
/// #[derive(TeraView)]
/// #[template = "user/show.html"]
/// pub struct Show<'a> {
///     #[partial]
///     pub base: Base<'a>,
///     pub user: User,
/// }
/// ```
///
/// This might generate the following `impl`:
///
/// ```
/// # use teratraits::{TeraView, TeraPartial};
/// # use serde::Serialize;
/// # #[derive(Serialize)]
/// # struct User;
/// # #[derive(TeraPartial)]
/// # pub struct Base<'a> {
/// #     title: &'a str,
/// # }
/// # pub struct Show<'a> {
/// #     pub base: Base<'a>,
/// #     pub user: User,
/// # }
/// impl<'a> TeraView for Show<'a> {
///     fn add_to(self, context: &mut tera::Context) {
///         self.base.add_to(context);
///         context.insert("user", &self.user);
///     }
///
///     const PATH: &'static str = "user/show.html";
/// }
/// ```
///
/// # Attributes
///
/// | [Item attribute](#item-attributes) | Short description                                |
/// | :---                               | :---                                             |
/// | [`partial(..)`](#partial)          | define a partial to add to the data inserted     |
/// | [`template =`](#template-)         | define the name/path of the Tera template to use |
///
/// | [Field attribute](#field-attributes)              | Short description              |
/// | :---                                              | :---                           |
/// | [`partial`](#partial-1)                           | define the field as a partial  |
/// | [`partial(..)`](#partial-2)                       | replace the field by a partial |
/// | [`exclude_from_template`](#exclude_from_template) | ignore the field               |
///
/// ## Item attributes
///
/// ### `partial(..)`
///
/// This attribute is used to render a partial which is not associated with a field.
///
/// For instance, if `Base` represents the base partial (extended by all templates), with the title
/// and an optional connected user, it is possible to render the `Signup` view assuming that no
/// user is connected as below:
///
/// ```
/// use teratraits::TeraView;
/// # use teratraits::TeraPartial;
/// # #[derive(TeraPartial)]
/// # struct Base(String); // Note: fields with no name are ignored by the derive macro
/// # impl Base { fn without_identity() -> BI { BI } }
/// # struct BI;
/// # impl BI { fn with_title(self, title: &str) -> Base { Base(title.to_string()) } }
///
/// #[derive(TeraView)]
/// #[template = "user/signup.html"]
/// #[partial(Base::without_identity().with_title("Sign Up"))]
/// pub struct Signup;
/// ```
///
/// Partials added this way are added to the context before the fields.
///
/// Any number of partials can be added this way.
///
/// ### `template =`
///
/// This attribute defines the name/path of the Tera template to use to render the view.
///
/// It is mandatory.
///
/// ## Field attributes
///
/// There can be zero or one attribute for each field.
///
/// ### `partial`
///
/// Declare the field as a partial so that it will not be inserted into the context as a template
/// variable but using [`.add_to(context)`](TeraPartial::add_to).
///
/// ### `partial(..)`
///
/// Do not add the field to the context as is, but add the partial provided in the attribute.
///
/// For instance, if `BaseIdentity` represents the base partial (extended by all templates), with
/// only the optional connected user, provided by the controller, it is possible to transform it
/// into a `Base` by adding a title to it, to render the `Show` view:
///
/// ```
/// use teratraits::TeraView;
/// # use serde::Serialize;
/// # #[derive(Serialize)]
/// # struct User { username: String };
/// # use teratraits::TeraPartial;
/// # #[derive(TeraPartial)]
/// # struct Base(String); // Note: fields with no name are ignored by the derive macro
/// # struct BaseIdentity;
/// # impl BaseIdentity { fn with_title(self, title: &str) -> Base { Base(title.to_string()) } }
///
/// #[derive(TeraView)]
/// #[template = "user/show.html"]
/// pub struct Show {
///     #[partial(self.base.with_title(&format!("{} - profile", self.user.username)))]
///     pub base: BaseIdentity,
///     // The user to show (not the connected user)
///     pub user: User,
/// }
/// ```
///
/// This way, the view is responsible for the title of the page but not for detecting if the user
/// is connected, which is the role of the controller.
///
/// ### `exclude_from_template`
///
/// Ignore the field when rendering views.
///
/// This attribute may be removed in the future because there may be no point adding a field to a
/// view `struct` without inserting in into the template.
pub use teratraits_derive::TeraView;

/// Derive macro to implement [`TeraPartial`].
///
/// - Fields without attributes are inserted into the Tera template context as variables named as
///   their field names.
/// - Partials are inserted using [`.add_to(context)`](TeraPartial::add_to).
///
/// # Examples
///
/// ```
/// use teratraits::TeraPartial;
/// use serde::Serialize;
///
/// // From the model
/// #[derive(Serialize)]
/// struct User;
///
/// #[derive(TeraPartial)]
/// struct Base<'a> {
///     title: &'a str,
///     connected_user: Option<User>,
/// }
/// ```
///
/// This might generate the following `impl`:
///
/// ```
/// # use teratraits::{TeraView, TeraPartial};
/// # use serde::Serialize;
/// # #[derive(Serialize)]
/// # struct User;
/// # struct Base<'a> {
/// #     title: &'a str,
/// #     connected_user: Option<User>,
/// # }
/// impl<'a> TeraPartial for Base<'a> {
///     fn add_to(self, context: &mut tera::Context) {
///         context.insert("_base__title", &self.title);
///         context.insert("_base__connected_user", &self.connected_user);
///     }
/// }
/// ```
///
/// # Attributes
///
/// | [Item attribute](#item-attributes) | Short description                                    |
/// | :---                               | :---                                                 |
/// | [`partial_name =`](#partial_name-) | define the partial name to include in variable names |
/// | [`partial(..)`](#partial)          | define a partial to add to the data inserted         |
///
/// | [Field attribute](#field-attributes)              | Short description              |
/// | :---                                              | :---                           |
/// | [`partial`](#partial-1)                           | define the field as a partial  |
/// | [`partial(..)`](#partial-2)                       | replace the field by a partial |
/// | [`exclude_from_template`](#exclude_from_template) | ignore the field               |
///
/// ## Item attributes
///
/// ### `partial_name =`
///
/// This attribute can be used to define the name of the partial.
///
/// When using `derive(TeraPartial)`, the fields are added to the Tera template context as
/// variables named with the convention `_{{partial_name}}__{{field_name}}`, so that they do not
/// get mixed with the view variables or with other partials variables.
///
/// Optional. Defaults to the lowercased name of the item.
///
/// ### `partial(..)`
///
/// This attribute is used to render a partial which is not associated with a field.
///
/// For instance, if `Base` is the base partial (extended by all templates), with the title and an
/// optional connected user, it is possible to define `SpecializedBase`, a specialized form of
/// `Base` (a partial extending `Base`), assuming that no user is connected, as below:
///
/// ```
/// use teratraits::TeraView;
/// # use teratraits::TeraPartial;
/// # #[derive(TeraPartial)]
/// # struct Base(String); // Note: fields with no name are ignored by the derive macro
/// # impl Base { fn without_identity() -> BI { BI } }
/// # struct BI;
/// # impl BI { fn with_title(self, title: &str) -> Base { Base(title.to_string()) } }
///
/// #[derive(TeraPartial)]
/// #[partial(Base::without_identity().with_title("My Blog"))]
/// pub struct SpecializedBase;
/// ```
///
/// Partials added this way are added to the context before the fields.
///
/// Any number of partials can be added this way.
///
///
/// ## Field attributes
///
/// There can be zero or one attribute for each field.
///
/// ### `partial`
///
/// Declare the field as a partial so that it will not be inserted into the context as a template
/// variable but using [`.add_to(context)`](TeraPartial::add_to).
///
/// ### `partial(..)`
///
/// Do not add the field to the context as is, but add the partial provided in the attribute.
///
/// For instance, if `Base` is the base partial (extended by all templates), with the title and an
/// optional connected user, it is possible to define `SpecializedBase`, a specialized form of
/// `Base` (a partial extending `Base`), assuming that no user is connected, as below:
///
/// ```
/// use teratraits::TeraPartial;
/// # #[derive(TeraPartial)]
/// # struct Base(String); // Note: fields with no name are ignored by the derive macro
/// # impl Base { fn without_identity() -> BI { BI } }
/// # struct BI;
/// # impl BI { fn with_title(self, title: &str) -> Base { Base(title.to_string()) } }
///
/// #[derive(TeraPartial)]
/// pub struct SpecializedBase<'a> {
///     #[partial(Base::without_identity().with_title(self.title))]
///     pub title: &'a str,
/// }
/// ```
///
/// ### `exclude_from_template`
///
/// Ignore the field when rendering views.
///
/// This attribute may be removed in the future because there may be no point adding a field to a
/// view `struct` without inserting in into the template.
pub use teratraits_derive::TeraPartial;

/// Trait representing a view defined in a Tera template file.
///
/// This trait can be implemented with [`derive(TeraView)`](teratraits_derive::TeraView).
///
/// # Examples
///
/// ```no_run
/// use teratraits::{TeraView, TeraPartial};
/// use serde::Serialize;
/// use tera::Tera;
///
/// // These `struct`s are part of the model, they implement `Serialize`
/// #[derive(Serialize)]
/// struct User;
/// #[derive(Serialize)]
/// struct Post;
/// #[derive(Serialize)]
/// struct Comment;
///
/// // Base is the base template, all views extend it. It implements TeraPartial.
/// #[derive(TeraPartial)]
/// struct Base;
///
/// // Show is a view ...
/// pub struct Show {
///     pub base: Base,
///     pub user: User,
///     pub posts: Vec<Post>,
///     pub comments: Vec<Comment>,
/// }
///
/// // ... so it implements TeraView ...
/// impl TeraView for Show {
///     const PATH: &'static str = "user/show.html";
///
///     fn add_to(self, context: &mut tera::Context) {
///         self.base.add_to(context);
///         context.insert("user", &self.user);
///         context.insert("posts", &self.posts);
///         context.insert("comments", &self.comments);
///     }
/// }
///
/// # fn test() -> Result<String, tera::Error> {
/// let mut tera = Tera::new("templates/**/*.html")?;
/// let (base, user, posts, comments) = (Base, User, Vec::new(), Vec::new());
/// // ... so it can be rendered using tera
/// let response = Show { base, user, posts, comments }.render_with(&tera)?;
/// # Ok::<String, tera::Error>(response)
/// # }
/// ```
pub trait TeraView: Sized {
    /// The path of the template file describing the view.
    const PATH: &'static str;

    /// Inserts fields of `Self` and the used partials to the given [`tera::Context`], as
    /// variables.
    ///
    /// `self` is taken by value because the rendering might require taking fields by value.
    fn add_to(self, context: &mut tera::Context);

    /// Renders the view using the given [`tera::Tera`] instance.
    ///
    /// `self` is taken by value because the rendering might require taking fields by value.
    fn render_with(self, tera: &tera::Tera) -> Result<String, tera::Error> {
        let mut context = tera::Context::new();
        self.add_to(&mut context);
        tera.render(Self::PATH, &context)
    }
}

/// Trait representing a partial defined in a Tera template.
///
/// This trait can be implemented with [`derive(TeraPartial)`](teratraits_derive::TeraPartial).
///
/// # Examples
///
/// ```no_run
/// use teratraits::{TeraView, TeraPartial};
/// use serde::Serialize;
/// use tera::Tera;
///
/// // These `struct`s are part of the model, they implement `Serialize`
/// #[derive(Serialize)]
/// struct User;
/// #[derive(Serialize)]
/// struct Post;
/// #[derive(Serialize)]
/// struct Comment;
///
/// // Base is the base template, all views extend it. It implements TeraPartial.
/// struct Base<'a> {
///     title: &'a str,
///     connected_user: Option<User>,
/// }
///
/// impl<'a> TeraPartial for Base<'a> {
///     fn add_to(self, context: &mut tera::Context) {
///         context.insert("_base__title", self.title);
///         context.insert("_base__connected_user", &self.connected_user);
///     }
/// }
///
/// #[derive(TeraView)]
/// #[template = "user/show.html"]
/// pub struct Show<'a> {
///     #[partial]
///     pub base: Base<'a>,
///     pub user: User,
///     pub posts: Vec<Post>,
///     pub comments: Vec<Comment>,
/// }
///
/// # fn test() -> Result<String, tera::Error> {
/// let mut tera = Tera::new("templates/**/*.html")?;
/// let base = Base {
///     title: "Hi!",
///     connected_user: None,
/// };
/// let (user, posts, comments) = (User, Vec::new(), Vec::new());
/// let response = Show { base, user, posts, comments }.render_with(&tera)?;
/// # Ok::<String, tera::Error>(response)
/// # }
/// ```
pub trait TeraPartial: Sized {
    /// Inserts fields of `Self` and the used partials to the given [`tera::Context`], as
    /// variables.
    ///
    /// `self` is taken by value because the rendering might require taking fields by value.
    fn add_to(self, context: &mut tera::Context);
}
