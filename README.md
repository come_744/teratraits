# Tera traits

Set of Rust `trait`s and derive macros to represent Tera views and partials.

The goal of these traits is to let the user define a `struct` for each view or
partial, implementing the appropriate `trait`, so that the `struct` provides a
type-safe interface which can be unit tested to match the data required by the
Tera template by simply rendering it.

This crate has initially been written in the context of the Rust on Waves
project (see the [draft](https://codeberg.org/come_744/Rust-on-Waves_draft)).

To understand how this crate can be useful, you can do one of the following
things:

- Read how it can be [integrated in the Rust on Waves draft project](https://codeberg.org/come_744/Rust-on-Waves_draft/pulls/15/files)
- Read [the test `main.rs` file](https://codeberg.org/come_744/teratraits/src/branch/master/teratraits_test/src/main.rs)
- Generate and read the documentation running `cargo doc --open` in the
  `teratraits/` folder.
