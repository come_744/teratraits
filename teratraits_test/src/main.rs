use teratraits::{TeraPartial, TeraView};

#[derive(TeraPartial)]
//#[partial_name = "base"]
pub struct Base<'a> {
    title: &'a str,
}

#[derive(TeraView)]
#[template = "user/login.html"]
#[partial(Base { title: "Login" })]
pub struct Login;

#[derive(TeraView)]
#[template = "user/index.html"]
pub struct List {
    #[partial(Base { title: "Index" })]
    pub base: (),
    pub users: Vec<()>,
}

#[derive(TeraView)]
#[template = "index.html"]
pub struct Index<'a> {
    #[partial]
    pub base: Base<'a>,
    #[exclude_from_template]
    pub posts_users: (),
}

fn main() {
    let tera = tera::Tera::new("templates/**/*.html").unwrap();
    let tera = &tera;

    let html = Login.render_with(tera).unwrap();
    println!("Login: {html}");

    let html = List {
        base: (),
        users: vec![],
    }
    .render_with(tera)
    .unwrap();
    println!("List: {html}");

    let html = Index {
        base: Base {
            title: "User profile",
        },
        posts_users: (),
    }
    .render_with(tera)
    .unwrap();
    println!("Index: {html}");
}
